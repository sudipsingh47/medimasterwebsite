(function() {
    "use strict";
    new Swiper('.testimonials-slider ', {
        speed: 1500,
        loop: false,

        autoplay: {
            delay: 700,
            disableOnInteraction: false
        },
        slidesPerView: 'auto',
        pagination: {
            el: ".swiper-pagination",

        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            clickable: true
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            },

            1200: {
                slidesPerView: 2,
                spaceBetween: 20
            }
        }
    });
    window.addEventListener('scroll', function() {
        var y = window.scrollY;
        var header = document.getElementById('header');

        if (y > 0) {
            header.classList.add('--not-top');
        } else {
            header.classList.remove('--not-top');
        }
    });


})()